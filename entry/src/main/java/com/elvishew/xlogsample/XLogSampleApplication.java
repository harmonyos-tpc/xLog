package com.elvishew.xlogsample;

import com.elvishew.xlog.LogConfiguration;
import com.elvishew.xlog.LogLevel;
import com.elvishew.xlog.XLog;
import com.elvishew.xlog.flattener.ClassicFlattener;
import com.elvishew.xlog.interceptor.BlacklistTagsFilterInterceptor;
import com.elvishew.xlog.printer.OhosPrinter;
import com.elvishew.xlog.printer.Printer;
import com.elvishew.xlog.printer.file.FilePrinter;
import com.elvishew.xlog.printer.file.naming.DateFileNameGenerator;
import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.render.render3d.BuildConfig;
import ohos.hiviewdfx.HiLog;

import java.io.File;
import java.io.IOException;

public class XLogSampleApplication extends AbilityPackage {
    public static Printer globalFilePrinter;

    @Override
    public void onInitialize() {
        super.onInitialize();
        HiLog.info(Constants.LABEL_LOG, "init_first");
        initXlog();
    }

    /**
     * Initialize XLog.
     */
    private void initXlog() {
        LogConfiguration config = null;
        try {
            config = new LogConfiguration.Builder()
                    .logLevel(BuildConfig.DEBUG ? LogLevel.ALL             // Specify log level, logs below this level won't be printed, default: LogLevel.ALL
                            : LogLevel.NONE)
                    .tag(getResourceManager().getResource(ResourceTable.String_global_tag).toString())                   // Specify TAG, default: "X-LOG"
                    // .t()                                                // Enable thread info, disabled by default
                    // .st(2)                                              // Enable stack trace info with depth 2, disabled by default
                    // .b()                                                // Enable border, disabled by default
                    // .jsonFormatter(new MyJsonFormatter())               // Default: DefaultJsonFormatter
                    // .xmlFormatter(new MyXmlFormatter())                 // Default: DefaultXmlFormatter
                    // .throwableFormatter(new MyThrowableFormatter())     // Default: DefaultThrowableFormatter
                    // .threadFormatter(new MyThreadFormatter())           // Default: DefaultThreadFormatter
                    // .stackTraceFormatter(new MyStackTraceFormatter())   // Default: DefaultStackTraceFormatter
                    // .borderFormatter(new MyBoardFormatter())            // Default: DefaultBorderFormatter
                    // .addObjectFormatter(AnyClass.class,                 // Add formatter for specific class of object
                    //     new AnyClassObjectFormatter())                  // Use Object.toString() by default
                    .addInterceptor(new BlacklistTagsFilterInterceptor(    // Add blacklist tags filter
                            "blacklist1", "blacklist2", "blacklist3"))
                    // .addInterceptor(new WhitelistTagsFilterInterceptor( // Add whitelist tags filter
                    //     "whitelist1", "whitelist2", "whitelist3"))
                    // .addInterceptor(new MyInterceptor())                // Add a log interceptor
                    .build();
        } catch (Exception e) {
            HiLog.error(Constants.LABEL_LOG,e.toString());
        }
        Printer ohosPrinter = null;             // Printer that print the log using android.util.Log
        Printer filePrinter = null;
        try {
            String file = new File(this.getExternalFilesDir("Documents") + "/xlogsample").getCanonicalPath();
            HiLog.info(Constants.LABEL_LOG, "sdcard filepath:" + file);
            ohosPrinter = new OhosPrinter();
            filePrinter = new FilePrinter                      // Printer that print the log to the file system
                    .Builder(file)   // Specify the path to save log file
                    .fileNameGenerator(new DateFileNameGenerator())        // Default: ChangelessFileNameGenerator("log")
                    // .backupStrategy(new MyBackupStrategy())             // Default: FileSizeBackupStrategy(1024 * 1024)
                    // .cleanStrategy(new FileLastModifiedCleanStrategy(MAX_TIME))     // Default: NeverCleanStrategy()
                    .flattener(new ClassicFlattener())                     // Default: DefaultFlattener
                    .build();
        } catch (IOException e) {
            HiLog.error(Constants.LABEL_LOG, e.toString());
        }

        XLog.init(                                                 // Initialize XLog
                config,                                                // Specify the log configuration, if not specified, will use new LogConfiguration.Builder().build()
                ohosPrinter,                                        // Specify printers, if no printer is specified, AndroidPrinter(for Android)/ConsolePrinter(for java) will be used.
                filePrinter);

        // For future usage: partial usage in MainActivity.
        globalFilePrinter = filePrinter;
    }
}
